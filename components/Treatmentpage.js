import React from 'react'
import {View,StyleSheet,Image,ScrollView,Linking,Text,Button} from 'react-native'

export default class Treatment_component extends React.Component{
    
   
    
    static navigationOptions = {
        title: 'Treatment',
      }
   
      
    render(){
        return(
           <ScrollView>
           <View style={{flex :1,backgroundColor:'white'}}>
           
           <View style={{borderWidth:3,borderRadius:10,marginBottom:10,marginLeft:10,marginRight:10,marginTop:10,borderColor:'#272d2d'}}>

           <View style={{borderWidth:3,borderRadius:10,borderColor:'black',backgroundColor:'#12355B',marginBottom:25,padding:5}}>
           <Text style = {style.TextHead}>{this.props.name}</Text>
           </View>
            
            <View>
            <Image source = {this.props.pic} style = {style.image_container}></Image>
            </View>

            </View>

            <View style={{ borderWidth: 3, borderRadius: 10, marginBottom: 10 ,marginLeft:10,marginRight:10,backgroundColor:'white',padding:5,borderColor:'#272d2d'}}>
            <View style={{ borderWidth: 3, borderRadius: 10, borderColor: 'black', backgroundColor: '#12355B', marginBottom: 5,padding:5}}>
                <Text style={style.TextHead}>Description</Text>
            </View>
            <Text style ={style.desc_container}>{this.props.desc}</Text>
            </View>

            <View>
                <Button title = "More for Treatment" onPress = {()=> Linking.openURL(this.props.web)}></Button>
            </View>
            </View>
            </ScrollView>
        )

    }

}

const style = StyleSheet.create({
       TextHead : {
            fontSize : 30,
            fontWeight : 'bold',
            textAlign : 'center',
            color:'white'

       },
    image_container : {
            height : 150,
            borderWidth: 1, 
            borderColor: 'black', 
            alignItems:'center',
            marginBottom : 20,
            paddingBottom:20
            
        },
        desc_container : {
            fontSize : 20,
            textAlign : 'center',
            marginRight : 5,
            marginLeft : 5,
            marginBottom : 20
        },
        web_container : {
            borderColor: 'black', 
            marginRight : 10,
            marginBottom : 10
        }
})