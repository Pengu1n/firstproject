import React from 'react';
import { Text, View,Image,StyleSheet} from 'react-native';
import {Icon} from 'react-native-elements'

export default class card extends React.Component {
    
  render() {

       return (
       
         <View style = {style.container}>
            
            <View style={style.imageContainer}>
            <Image style={{ height: 180,borderRadius:10,borderWidth:3,borderColor:'#272d2d' }}  source = {this.props.img}/>
            </View>
            
            <View style = {style.fontContainer}>
            <Text style = {{fontSize : 18 , fontWeight : 'bold'}}>Patient Name: {this.props.name} </Text>
            
            <Text style={{color:'red',fontSize : 18}}>Disease : {this.props.date}</Text>
            <Text style={{fontSize:14}}>{this.props.desc}</Text>
            
            </View> 
         
         </View>
        
       );
    }
  }

  const style = StyleSheet.create({
    container : {
      flex: 1, 
      flexDirection: 'row', 
      borderWidth: 3, 
      borderColor: '#272d2d',
      borderRadius: 10, 
      marginTop: 10, 
      backgroundColor: 'white', 
      marginLeft: 10, 
      marginRight: 10
    },
    imageContainer:{
      flex: 1, 
    },
    fontContainer:{
      flex:2,
      paddingLeft:10,
    }

  })