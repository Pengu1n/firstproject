import React from 'react'
import { Button, ScrollView, View ,Text } from 'react-native'
import firebase from 'firebase'
import ViewUser from '../../components/viewUser'
import { StackActions, NavigationActions } from 'react-navigation';
import {Icon} from 'react-native-elements';

export default class user_home extends React.Component {
    //update
    constructor(props) {
        super(props)
        this.state = { data: [], status: false }
    }
    static navigationOptions = {
        title: 'Home of User',
    }
    componentDidMount() {
        var uid = firebase.auth().currentUser.uid
        firebase.database().ref('user/' + uid)
            .on('value', (snapshot) => {
                var data = snapshot.val()
                if (data !== null) {
                    this.setState({ data: snapshot.val(), status: true })
                } else {
                    this.props.navigation.navigate('usersetting')
                }
            })
        

    }
    renderItem(Id) {
        var name = ''
        if (Id == 0) {
            name = "Carcinoma"
        } else if (Id == 1) {
            name = "Diabetes"
        } else if (Id == 2) {
            name = "Renal failure"
        } else if (Id == 3) {
            name = "Heart disease"
        }

        return name
    }
    renderButton() {
        if (this.state.status) {
            return (
                <View style={{marginBottom:0}}>
                <Button style={{ padding: 50 }}
                    title='Treatment'
                    onPress={() => this.props.navigation.navigate('Treatment', { id: this.state.data.date })} />
                </View>  
            )
        }
    }

    rendertime() {
        if (this.state.status) {
            return (
                <View style={{ flex: 1, flexDirection: 'row', padding: 5,backgroundColor:'#12355b'}}>
                    <View style={{ borderWidth: 2, borderColor: '#272d2d', padding: 4, backgroundColor: 'white'}}>
                <Icon 
                type='antdesign'
                name='calendar'
                size={24}
                color='black'
                />
                </View>    
                <Text
                    style={{ fontSize: 20, fontWeight: 'bold',paddingTop:8,paddingLeft:5,color:'white'}}>
                    Your next appointment : {this.state.data.time}
                </Text>
                </View>
            )
        }
    }
    
   signout(){
    firebase.auth().signOut().then(()=>{
        alert('Sign out !')
        const resetAction = StackActions.reset({
            index: 0,
            actions: [NavigationActions.navigate({ routeName: 'Login' })],
        });
        this.props.navigation.dispatch(resetAction);
    })
    
   }
   
    render() {
        
        return (
            <ScrollView>

                <View>
                    {this.rendertime()}
                    <View style={{ borderWidth: 1, borderColor: 'black', padding: 3, backgroundColor:'#E8F4FA'}}>
                    <View style={{
                        borderWidth: 1,
                        borderColor: 'black',
                        backgroundColor:'#FF4500',
                        flex: 1,
                        flexDirection: 'row',
                        marginRight: 10,
                        marginLeft: 235,
                        justifyContent: 'center',
                        height: 36,
                        marginTop:5 ,
                        marginBottom:5,
                        borderRadius: 5,
                        padding:3}}>
                    {/* <Button title='Sign Out' onPress={() => this.signout()}></Button> */}
                            
                        
                        <View style={{ marginLeft: 10 }}>

                            <Icon
                                name='sign-out'
                                type='font-awesome'
                                size={24}
                                color='white'
                                onPress={() => this.signout()}
                            />

                        </View>
                        <View style={{ marginRight: 10 }}>
                            <Text style={{
                                paddingTop: 2,
                                paddingLeft: 8,
                                color: 'white',
                                fontSize: 14,
                                }}
                                onPress={() => this.signout()}
                            >
                                Sign out
                            </Text>
                        </View>

                    </View>            
                    
                    </View>
                    
                    <ViewUser name={this.state.data.first_name}
                        desc={this.state.data.desc}
                        date={this.renderItem(this.state.data.date)}
                        pic = {{uri : this.state.data.pic}} />

                    <View style={{marginTop:50}}>
                    {this.renderButton()}
                    </View>


                </View>
            </ScrollView>
        )
    }

}