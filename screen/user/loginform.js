import React from 'react'
import {Text,View,Button,TextInput,ActivityIndicator,KeyboardAvoidingView,Image} from 'react-native'
import {logincontainner} from '../../style/loginForm'
import firebase from 'firebase'
import {Icon} from 'react-native-elements'


export default class login extends React.Component{
    constructor(props){
        super(props)
        this.state = {email : '',password : '',error : '',loading:false}
    }
      onButtonPress() {
        this.setState({ error: '', loading: true })
        const { email, password } = this.state;
        if(this.state.email == 'admin' && this.state.password == '1234'){
          this.props.navigation.navigate('Home')
        }else{
          firebase.auth().signInWithEmailAndPassword(email, password)
          .then(() => this.props.navigation.navigate('User_home'))
          .catch((error) => {
            this.setState({error : 'You do not have account or wrong password',loading:false,password:''})
            
          });
        }
       
      }
     
      
      renderButton() {
        if (this.state.loading) {
          return(
              <View >
                 <ActivityIndicator size={"small"} />
              </View>
          );
        }
        return (
          <Button
            title="Login"
            onPress={this.onButtonPress.bind(this)} 
        
            />
        );
      }
      render(){
        return(
      
          <KeyboardAvoidingView  behavior="padding" enabled>
            
            <View style={logincontainner.logo}>
              <Text style={logincontainner.logoFont}>Dr.Naruk</Text>
            </View>             

            <View style={{alignItems:'center'}}>
              <Image
                style={{marginTop:30,width: 210, height: 150 }}
                source={{ uri: 'https://firebasestorage.googleapis.com/v0/b/mongkhonbase.appspot.com/o/logo.PNG?alt=media&token=33de06eb-888c-4e50-bff1-2c81ae3ac563' }}
              />
             </View> 

            <View style={logincontainner.container}>
              
            <Text style={logincontainner.text}>Email</Text>
            <View style ={logincontainner.buttomStyle}>
                <View style={logincontainner.iconBorder}>
                <Icon
                  name='at'
                  type='font-awesome'
                  size={24}
                  color='#272d2d'
              />
              </View>
            <TextInput style={logincontainner.input} placeholder='user@email.com' value={this.state.email} onChangeText = {(email) => this.setState({email : email})}></TextInput>
            
            </View>

            <Text style={logincontainner.text}>Password</Text>
            <View style={logincontainner.buttomStyle}>
            <View style={logincontainner.iconBorder}>
                  <Icon style={logincontainner.icon}
                    name='key'
                    type='font-awesome'
                    size={24}
                    color='#272d2d'
                  />
            </View>
            <TextInput style={logincontainner.input} secureTextEntry={true} value={this.state.password} onChangeText = {(password) => this.setState({password:password})} placeholder = 'password'></TextInput>
            
            
            </View>

            <Text style={logincontainner.errorTextStyle}>
                {this.state.error}
              </Text>

            </View>
            <View style={logincontainner.buttonContainer}>
            {this.renderButton()}   

            <Text style={{ marginBottom: 10 }}></Text> 
            </View>
        </KeyboardAvoidingView>
       
            
        )
    }

}