import React from 'react';
import { Text, View, ScrollView, Button, TextInput,StyleSheet} from 'react-native';
import ModalDropdown from 'react-native-modal-dropdown';
import firebase from 'firebase'
import {Icon} from 'react-native-elements';


//update
export default class settingUser extends React.Component {

    constructor(props) {
        super(props)
        const email = firebase.auth().currentUser.email
        this.state = { pdate :'',pdesc: '',pname:'',email:email,error:''}
    }

    submitOrder(){
        firebase.database().ref('user/'+firebase.auth().currentUser.uid).set({
            email: this.state.email,
            date: this.state.pdate,
            desc: this.state.pdesc,
            first_name: this.state.pname,
            pic: 'https://lh3.googleusercontent.com/Szq_YQxTW7swO9nEIxAZ56QD_3LHns2K42SZMTpW7_7H9xkY9dEuEPKUmuv3C52dm7cmjTKxPP-wi0kfBpwkL0GwOKqGP8yZyg0=s0',
            time: ''
        }).then(()=>this.props.navigation.goBack())

    }

    renderItem(){
       return( 
        <View style={{padding:5}}>
        <ModalDropdown  
        textStyle = {fontSize = 15}
        defaultValue = 'Select your Desease '
        onSelect = {(date) => this.setState({pdate : date})}
        options={['Carcinoma', 'Diabetes','Renal failure','Heart disease']}/>
        </View>
       )
    }

    renderButton(){
       
        if(this.state.pdate.trim() == ''){
            return(
               <Text style={{fontSize:20}}> You need to fill all information !</Text>
            )
        }else{
           return(<Button title = 'Add data' onPress={()=>this.submitOrder()}></Button>) 
        }
    }

    render() {
        let stylex = {
            borderWidth: 1, borderColor: 'gray', marginBottom: 5
        }

       

        return (
            <ScrollView>
                <View style={{ padding: 20}}>
                    
                    <Text style={{ fontSize: 20, fontWeight: 'bold' }}>
                    Setting 
                    </Text>

                    <View style={style.container}>
                        <View style={style.iconBorder}>
                            <View style={{ paddingTop: 2 }}>
                                <Icon
                                    type='antdesign'
                                    name='user'
                                    size={24}
                                    color='black'
                                />
                            </View>
                        </View>
                        <TextInput style={style.inputContainer}
                            placeholder='Name' onChangeText={(name) => this.setState({ pname: name })} ></TextInput>
                    </View>

                   
                    <View style={style.container}>
                        <View style={style.iconBorder}>
                            <View style={{ paddingTop: 2 }}>
                                <Icon
                                    type='fontawesome'
                                    name='info'
                                    size={24}
                                    color='black'
                                />
                            </View>
                        </View>
                        <TextInput style={style.inputContainer}
                            placeholder='Desc' onChangeText={(desc) => this.setState({ pdesc: desc })}></TextInput>
                    </View>

                    
                    <View style={style.container}>
                        <View style={style.iconBorder}>
                            <View style={{ paddingTop: 2 }}>
                                <Icon
                                    type='fontawesome'
                                    name='email'
                                    size={24}
                                    color='black'
                                />
                            </View>
                        </View>
                        <TextInput style={style.inputContainer}
                            value={this.state.email} editable={false}></TextInput>
                    </View>    

                    

                    <View style={style.container}>
                        <View style={style.iconBorder}>
                            <View style={{ paddingTop: 2 }}>
                                <Icon
                                    type='fontawesome'
                                    name='email'
                                    size={24}
                                    color='black'
                                />
                            </View>
                        </View>
                        <View style={style.inputContainer}>
                            {this.renderItem()}
                        </View>
                    </View> 

                   
                   
                    <View style={{ borderWidth: 1, backgroundColor: '#ffe6ff' }}>
                        {this.renderButton()}
                    </View>
                </View>
            </ScrollView>
        )
    }
}

const style = StyleSheet.create({
    container: {
        marginTop: 10,
        marginBottom: 10,
        flex: 1,
        flexDirection: 'row'
    },

    inputContainer: {
        fontSize: 15,
        padding: 10,
        flex: 1,
        backgroundColor: '#E8F4FA',
        borderRadius: 10,
        borderWidth: 3,
        borderColor: '#5DAFDB',
        marginLeft: 5
    },
    iconBorder: {
        borderColor: '#5DAFDB',
        borderWidth: 2,
        width: 50,
        height: 54,
        paddingTop: 10,
        backgroundColor: '#A2D1EA',
        borderRadius: 10
    }
    
})

