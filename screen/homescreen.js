import React from 'react';
import { ScrollView, FlatList,View,Button,Text,StyleSheet} from 'react-native';
import Card from '../components/card';
import firebase from 'firebase';
import { TouchableOpacity } from 'react-native-gesture-handler';
import {style} from '../style/homescreen';
import { StackActions, NavigationActions } from 'react-navigation';
import {Icon} from 'react-native-elements';

export default class Home extends React.Component {
  static navigationOptions = {
    title: 'List of Patients',
  }

  constructor(props) {
    super(props)
    this.state = { data: [] }
  }
  
  

  componentDidMount() {
    firebase.database().ref('user').on('value', (snapshot) => {
        let data = snapshot.val()
        var defaultdata = {
          email: '',
          date: '',
          desc: '',
          first_name: '',
          pic: '',
          time: ''
        }
  
        if(data == null){
          this.setState({data : defaultdata})
        }else{
          let items = Object.values(data)
          this.setState({ data: items })
      
        }
        
    }
  
    )
}
//asd
  // deleteuser(name) {
   
  //   firebase.database().ref('user').orderByChild('first_name').equalTo(name).once('child_added',
  //   (snapshot) => {
  //       firebase.database().ref('user').child(snapshot.key).remove()  
  //   })
  // }
  
  callupdate(item) { 
  this.props.navigation.navigate('Update', {itemx:item })
}

signout(){
      const resetAction = StackActions.reset({
          index: 0,
          actions: [NavigationActions.navigate({ routeName: 'Login' })],
      });
      this.props.navigation.dispatch(resetAction);
  
  
 }

  render() {
    return (

      <ScrollView style={style.background}>
        
        <View style={{ backgroundColor:'#E8F4FA',paddingTop:5,paddingBottom:5,borderWidth:3,borderColor:'#272d2d'}}>
        <View style={style.buttonContainer}>
          <View style={{ marginLeft: 10 }}>

            <Icon
              name='sign-out'
              type='font-awesome'
              size={24}
              color='white'
              onPress={() => this.signout()}
            />

          </View>
          <View style={{ marginRight: 10 }}>
            <Text style={style.buttonText}
              onPress={() => this.signout()}
            >
              Sign out
          </Text>
          </View>

        </View>
        </View>

        <View >
        
        <FlatList data={this.state.data}
        renderItem={({ item }) => 
          <TouchableOpacity 
            onPress={() => this.callupdate(item)}>
            
            <Card name={item.first_name} 
            date={item.date} 
            img={{ uri: item.pic }} 
            desc={item.desc}
            />

            </TouchableOpacity>
        }
        />
        
        <View style={style.createContainer}>
        {/* <Button
        title ='Creat User' 
        onPress={()=> this.props.navigation.navigate('Setting')}
        /> */}
        <View style={{ marginLeft: 10 }}>

          <Icon
            name='user-plus'
            type='font-awesome'
            size={22}
            color='white'
            onPress={() => this.props.navigation.navigate('Setting')}
          />

        </View>
        <View style={{marginRight:10}}>
          <Text style={style.buttonText}
            onPress={() => this.props.navigation.navigate('Setting')}
          >
          Add User
          </Text>
        </View>
            
        </View>

        {/* <View style={{paddingTop:10}}>
        <Button title ='Sign out' onPress={()=> this.signout()}></Button>
        </View> */}
        
        </View>


      </ScrollView>
      
    );
  }

}



