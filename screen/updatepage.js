import React from 'react';
import { Text, View, ScrollView, Button, TextInput,StyleSheet } from 'react-native';
import firebase from 'firebase'
import {Icon} from 'react-native-elements';


export default class update extends React.Component {
    static navigationOptions = {
        title: 'Update Schedule',
    }
    constructor(props) {
        super(props)
        const item = this.props.navigation.state.params.itemx
        this.state = {item : item , time : '' }
        
    }
    updateuser(item) {
        var newData = {
            time : this.state.time
          }
        firebase.database().ref('user').orderByChild('first_name').equalTo(item.first_name).once('child_added',
            (snapshot) => {
                firebase.database().ref('user').child(snapshot.key).update(newData).then(function(){
                    alert("Data saved successfully.")
                }).catch(function(error){
                    alert(error)
                })
                
            })
            this.props.navigation.goBack()

    }

    render() {
        let stylex = {
            borderWidth: 1, borderColor: 'gray', marginBottom: 5
        }

        return (
            <ScrollView>
                <View style={{ padding: 20 }}>
                    <Text style={{ fontSize: 20, fontWeight: 'bold',marginBottom:20 }}> Update </Text>

                    <View style={style.container}>
                        <View style={style.iconBorder}>
                            <View style={{ paddingTop: 2 }}>
                                <Icon
                                    type='antdesign'
                                    name='calendar'
                                    size={24}
                                    color='black'
                                />
                            </View>
                        </View>
                        <TextInput style={style.inputContainer} value={this.state.time} 
                        onChangeText={(time) => this.setState({ time: time })} placeholder='Time'></TextInput>
                    </View>

                    <View style={{ borderWidth: 1, backgroundColor: '#ffe6ff' }}>
                        <Button onPress={() => this.updateuser(this.state.item)} title="Update" />
                    </View>
                </View>
            </ScrollView>
        )
    }
}
const style = StyleSheet.create({
    container : {
        marginTop: 10,
        marginBottom: 10,
        flex: 1,
        flexDirection: 'row'
    },
    inputContainer:{
        fontSize: 15,
        padding: 10,
        flex: 1,
        backgroundColor: '#E8F4FA',
        borderRadius: 10,
        borderWidth: 3,
        borderColor: '#5DAFDB',
        marginLeft: 5
    },
    iconBorder:{
        borderColor: '#5DAFDB',
        borderWidth: 2,
        width: 50,
        height: 54,
        paddingTop: 10,
        backgroundColor: '#A2D1EA',
        borderRadius: 10
    }
})