import React from 'react';
import { Text, View, ScrollView, Button, TextInput,ActivityIndicator,StyleSheet} from 'react-native';
import firebase from 'firebase'
import { Icon } from 'react-native-elements';

export default class setting extends React.Component {
    static navigationOptions = {
        title: 'Create User',
    }

    constructor(props) {
        super(props)
        this.state = {email:'',password:'',loading:false,error:''}
    }

    submitOrder() {
        const {email , password} = this.state
        this.setState({loading:true})
       firebase.auth().createUserWithEmailAndPassword(email,password).then(()=>this.props.navigation.goBack())
       .catch((error)=>{
        let errorCode = error.code
        let errorMessage = error.message;
        if (errorCode == 'auth/weak-password') {
          this.onLoginFailure.bind(this)('Weak password!')
        } else {
          this.onLoginFailure.bind(this)(errorMessage)
        }
       })
    }
    onLoginFailure(errorMessage) {
        this.setState({ error: errorMessage, loading: false })
      }

    renderButton(){
        if(this.state.loading){
            return(
                <ActivityIndicator />
            )
        }else{
            return(
                <Button onPress={() => this.submitOrder()}title="Add data"/>
            )
        }
    }

    render() {
        let stylex = {
            borderWidth: 1, borderColor: 'gray', marginBottom: 5
        }

       

        return (
            <ScrollView>

                <View style={{ padding: 20 }}>
                   
                    <Text style={{ fontSize: 20, fontWeight: 'bold' }}>
                     Create User 
                     </Text>

                    <View style={style.container}>
                        <View style={style.iconBorder}>
                            <View style={{paddingTop:2}}>
                                <Icon
                                    type='fontawesome'
                                    name='email'
                                    size={24}
                                    color='black'
                                />
                            </View>
                        </View>
                        <TextInput style={style.inputContainer}
                            placeholder='1234@hotmail.com' onChangeText={(email) => this.setState({ email: email })} ></TextInput>
                    </View>

                    <View style={style.container}>
                        <View style={style.iconBorder}>
                            <View style={{ paddingTop: 2 }}>
                                <Icon
                                    type='antdesign'
                                    name='key'
                                    size={24}
                                    color='black'
                                />
                            </View>
                        </View>
                        <TextInput style={style.inputContainer} 
                        placeholder='password' onChangeText={(password) => this.setState({ password: password })}></TextInput>
                    </View>
                   
                    <View>
                        <Text style={{ color: 'red' }}>{this.state.error}</Text>
                    </View>

                    <View style={{ borderWidth: 1, height:36}}>
                        {this.renderButton()}
                    </View>


                </View>

            </ScrollView>
        )
    }
    
}

const style = StyleSheet.create({
    container: {
        marginTop: 10, 
        marginBottom: 10,
        flex:1,
        flexDirection:'row'
    },

    inputContainer : {
        fontSize: 15, 
        padding: 10,
        flex: 1,
        backgroundColor: '#E8F4FA',
        borderRadius: 10,
        borderWidth: 3,
        borderColor: '#5DAFDB',
        marginLeft:5
    },
    iconBorder:{
        borderColor: '#5DAFDB',
        borderWidth: 2,
        width: 50,
        height: 54,
        paddingTop: 10,
        backgroundColor: '#A2D1EA',
        borderRadius: 10
    },
})
