import {StyleSheet} from 'react-native'

export const logincontainner = StyleSheet.create({
    container : {
        borderWidth : 0.5,
        borderColor : '#178CCB',
        marginTop:30,
        alignItems:'center',
        backgroundColor : '#272d2d',
        marginLeft:10,
        marginRight:10,
        borderRadius:10
    },
    text : {
        fontSize:18,
        fontWeight:'bold',
        color:'white'
    },
    errorTextStyle: {
        fontSize: 14,
        alignSelf: 'center',
        color: 'red'
      },
      buttomStyle : {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#272d2d',
        // borderWidth: 0.5,
        borderRadius:10,
        height: 40,
        margin: 10,
      },
      input:{
        // Effect only insdide text input
        padding:10,
        flex:1,
        backgroundColor:'#E8F4FA',
        // borderWidth:1,
        borderRadius:10,
        borderWidth:3,
        borderColor:'#12355B'
      },
      icon : {
        borderColor:'#5DAFDB',
       borderWidth:1
      },
      iconBorder:{
        borderColor: '#12355B',
         borderWidth: 2,
         width: 50, 
         height: 40, 
         paddingTop: 5, 
         backgroundColor: '#A2D1EA',
         borderRadius:10,
      },
      buttonContainer:{
        // backgroundColor: '#178CCB',
        padding:10,
        
      },
      logo:{
        padding:15,
        alignItems: 'center',
        backgroundColor: '#272d2d',
        marginLeft: 10,
        marginRight: 10,
        borderRadius:10,
        marginTop:30
      },
      logoFont:{
        fontSize:24,
        fontWeight:'bold',
        color:'white'
      }
})

