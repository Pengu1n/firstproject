import {StyleSheet} from 'react-native'

export const style = StyleSheet.create({
    background : {
        backgroundColor : 'white'
    },
    createUser:{
        flex:1,
        marginLeft : 250,
        marginRight : 10,
        paddingTop:10
    },
    buttonContainer: {
        borderWidth: 1,
        backgroundColor: '#FF4500',
        flex: 1,
        flexDirection: 'row',
        marginRight: 10,
        marginLeft: 240,
        justifyContent: 'center',
        height: 36,
        // marginTop: ,
        borderRadius: 10,
        paddingTop: 5
    },
    buttonText:{
        paddingTop: 2,  
        paddingLeft: 8, 
        color: 'white', 
        fontSize: 14, 
        
    },
    createContainer:{
            borderWidth: 1,
            backgroundColor: '#1E90FF',
            flex: 1,
            flexDirection: 'row',
            marginRight: 10,
            marginLeft: 240,
            justifyContent: 'center',
            height: 36,
            marginTop: 10,
            marginBottom:10,
            borderRadius: 10,
            paddingTop: 5   
    }
    
})